#!/usr/bin/env python3

import os
import shutil
import subprocess
import sys


def main():
    args = sys.argv[1:]

    subprocess.run(
        [shutil.which("mksquashfs")] + args + [
            "-comp", "zstd",
            "-not-reproducible",
            "-root-uid", str(os.getuid()),
            "-root-gid", str(os.getgid()),
            # This is the default value used by mksquashfs
            "-Xcompression-level", "15"
        ]
    )


if __name__ == "__main__":
    main()
