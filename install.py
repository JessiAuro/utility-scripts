#!/usr/bin/env python3

import os
import shutil


def main():
    install_path = os.path.join(os.environ['HOME'], ".local", "bin")
    self_path = os.path.realpath(__file__)
    script_path = os.path.join(os.path.dirname(self_path), "scripts")

    if install_path not in os.environ['PATH']:
        warn_path()

    if not os.path.isdir(install_path):
        os.makedirs(install_path)

    with os.scandir(script_path) as dir:
        for item in dir:
            path = shutil.copy(item, install_path)
            os.chmod(path, 0o755)


def warn_path():
    print(  """
            WARNING: Install destination not present in $PATH.

            You may need to log out and/or modify your profile to use the
            installed scripts.

            See README.md for details.
            """)


if __name__ == "__main__":
    main()
