# Utility Scripts

This repo hosts single-file utility scripts for simplifying/automating various tasks.

## Installation

To use these scripts, simply run `install.py`. This will install all scripts into `$HOME/.local/bin`. Most distributions will add this to your `$PATH` by default, but if `$HOME/.local/bin` didn't exist prior to running `install.py` you will have to log out and log back in to update your `$PATH`.

If the scripts still aren't usable after running `install.py` and logging out, you can manually add the necessary location to your `$PATH` with a snippet like this in `$HOME/.bash_profile`, `$HOME/.bash_login`, **OR** `$HOME/.profile` depending on which file exists, in that order of precedence:

```shell
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi
```

## Usage

| Script | Dependencies | Usage | Example | Description |
| ------ | ------------ | ----- | ------- | ----------- |
| `squish.py` | `gensquashfs` | `squish.py <source> [<source> …] <output-file>` | `squish.py ./mydir/ ./output.sqsh` | Simplifies using SquashFS images for archiving by setting sensible default parameters. |
